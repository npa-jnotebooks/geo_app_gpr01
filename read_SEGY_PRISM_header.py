import struct


def npa_read_coord_in_SEGY_PRISM_header(GPRfile,ntraces):
    
    gpr_lon = []
    gpr_lat = []
    gpr_mark = []
    
    
    with open(GPRfile,"rb") as f:
        data = f.read()

    #print(data[0:3200])
    unpack_result = sum(struct.unpack('h', data[3224:3226]))
    print('\nEncoding Byte Type: ', unpack_result)
    unpack_result = sum(struct.unpack('h', data[3216:3218]))
    print('Delta (in picos): ', unpack_result)
    unpack_result = sum(struct.unpack('h', data[3220:3222]))
    print('Nsamples: ', unpack_result)
    print('\n')


    il=0
    while il < ntraces:
        h_start=3600+il*(240+1024*2)
        lon1 = sum(struct.unpack('d', data[h_start+182:h_start+190]))
        lat1 = sum(struct.unpack('d', data[h_start+190:h_start+198]))
        mark = sum(struct.unpack('h', data[h_start+238:h_start+240]))
        if mark != 0:
            print('%6d%s%16.8f%16.8f%s%6d' % (il, '  LON/LAT: ', lon1, lat1, '  MARK: ',  mark))
        if il % 500 == 0:
            print('%6d%s%16.8f%16.8f%s%6d' % (il, '  LON/LAT: ', lon1, lat1, '  MARK: ',  mark))
        gpr_lon.append(lon1)
        gpr_lat.append(lat1)
        gpr_mark.append(mark)
        il += 1
    

    
    return gpr_lon, gpr_lat, gpr_mark

