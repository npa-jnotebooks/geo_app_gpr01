import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import figure

def sinewave(Massa = 10, μs = 0.2):
    
    Vp = [13.88888888888889, 19.444444444444443, 30.555555555555554, 36.11111111111111] 
    g = 9.80665
            
    μ1 = μs/pow(Massa, 1/3)
      
    k = 0  
    sp2 = []
    while k < 4: 
        sp1 = (pow(Vp[k],2)) / (g*μ1*2)
        um = "metri"
        sp2.insert(4, sp1)
        k = k + 1

    v = 50, 70, 110, 130
    s = sp2

    plt.ylim(0,1200)
    plt.plot(v, s)
    plt.grid()
    plt.ylabel('spazio di frenata (m)')
    plt.xlabel('velocità (km/h)')
    plt.title('grafico spazio di frenata-velocità')
    plt.figure()
    return 
